﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
	public GameObject inventoryMenu;
	public GameObject background;
	public GameObject player;
	public GameObject tileSet;
    private GameObject[] enemies;
    private GameObject[] items;
    public GameObject gameOverCanvas;
    public GameObject playerBlood; //TODO
    public GameObject uiPlayerStats;
    public GameObject lifebarCanvas;
    private string startText = "I'm gonna trample everything!";

	public GameObject interactionText;
	public float timeLeft = 2f;

	GameObject[] FindGameObjectsInLayer(int layer)
    {
        var goArray = FindObjectsOfType(typeof(GameObject)) as GameObject[];
        var goList = new System.Collections.Generic.List<GameObject>();
        for (int i = 0; i < goArray.Length; i++)
        {
            if (goArray[i].layer == layer)
            {
                goList.Add(goArray[i]);
            }
        }
        if (goList.Count == 0)
        {
            return null;
        }
        return goList.ToArray();
    }

    public void EnableDisableGameObjectByLayer(GameObject[] group, bool enabled)
    {
        //enable enemies
        if(group.Length != 0)
        {
            for (int i = 0; i < group.Length; i++)
            {
                if(group[i]) group[i].gameObject.SetActive(enabled);
            }
        }
    }

    private void Start()
    {
        inventoryMenu.gameObject.SetActive(false);
        enemies = FindGameObjectsInLayer(LayerMask.NameToLayer("Enemy"));
        items = FindGameObjectsInLayer(LayerMask.NameToLayer("Item"));
        ShowInteractionText(startText);
    }

    public void ShowInteractionText(string text)
    {
        interactionText.SetActive(true);
        interactionText.transform.GetComponent<Text>().text = text;
        Invoke("CleanText", 4f);
    }

    private void CleanText()
    {
        interactionText.transform.GetComponent<Text>().text = "";
    }

    private void Update()
    {
        inventoryControl();
    }

    private void inventoryControl()
    {
    	if(Input.GetButtonDown("OpenInventory"))
    	{
    		if(GameManager.instance.isPaused) Resume(); //TODO GameManager.instace null
    		else Pause();
    	}
    }

    private void Resume()
    {
    	inventoryMenu.gameObject.SetActive(false);
    	Time.timeScale = 1.0f;
    	GameManager.instance.isPaused = false;

        
        EnableDisableGameObjectByLayer(enemies, true);
    	if(background.gameObject) background.gameObject.SetActive(true);
    	//cause bug
        //if(player.gameObject) player.gameObject.SetActive(true);
        player.gameObject.transform.GetComponent<SpriteRenderer>().enabled = true;
        EnableDisableGameObjectByLayer(items, true); //bug of trying to activate a destroyed items
        if(tileSet.gameObject) tileSet.gameObject.SetActive(true);
        if(lifebarCanvas.gameObject) lifebarCanvas.gameObject.SetActive(true);
        if(interactionText.gameObject) interactionText.gameObject.SetActive(true);
    }

    private void Pause()
    {
    	inventoryMenu.gameObject.SetActive(true);
    	Time.timeScale = 0.0f;
    	GameManager.instance.isPaused = true;

        
        EnableDisableGameObjectByLayer(enemies, false);
    	if(background.gameObject) background.gameObject.SetActive(false);
    	//cause bug
        //if(player.gameObject) player.gameObject.SetActive(false);
        player.gameObject.transform.GetComponent<SpriteRenderer>().enabled = false;
        player.transform.GetChild(5).GetComponent<SpriteRenderer>().enabled = true; //TODO bloodanimation
        EnableDisableGameObjectByLayer(items, false);
        if(tileSet.gameObject) tileSet.gameObject.SetActive(false);
        if(lifebarCanvas.gameObject) lifebarCanvas.gameObject.SetActive(false);
        if(interactionText.gameObject) interactionText.gameObject.SetActive(false);

    }

    public void GameOverScreen()
    {
        //GameManager.instance.isPaused = true;
        EnableDisableGameObjectByLayer(enemies, false);
        if(background.gameObject) background.gameObject.SetActive(false);
        player.gameObject.transform.GetComponent<SpriteRenderer>().enabled = false;
        player.transform.GetChild(5).GetComponent<SpriteRenderer>().enabled = false; //TODO bloodanimation
        EnableDisableGameObjectByLayer(items, false);
        if(tileSet.gameObject) tileSet.gameObject.SetActive(false);
        if(uiPlayerStats.gameObject) uiPlayerStats.gameObject.SetActive(false);
        if(lifebarCanvas.gameObject) lifebarCanvas.gameObject.SetActive(false);
        if(interactionText.gameObject) interactionText.gameObject.SetActive(false);

    
    }

}