﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour
{
	//patrol
	public float speed;
	private float startSpeed;
	public float distanceToFloor;
	private bool movingRight = true;
	public Transform groundDetection;
    private bool isCloseToPlayer;
    public GameObject enemyPatrol;

	//aggro
	[SerializeField]
	Transform player;
	[SerializeField]
	float aggroRange;

	public Animator animator;
	public float attackRange = 2f;
	public Transform aggroDetection;
    private bool aggroState = false;

    private float leftLimitPositionX;
    private float rightLimitPositionX;

	void Start()
	{
		startSpeed = speed;
        enemyPatrol = gameObject;
	}

    public bool GetAggroState()
    {
        return aggroState;
    }

    bool LookingToPlayer()
    {
        if(transform.position.x < player.position.x)//left of player
        {
            if(movingRight) return true;
            else return false;
        }
        else//right of player 
        {
            if(movingRight) return false;
            else return true;
        }
    }

    public bool GetIsCloseToPlayer()
    {
        return isCloseToPlayer;
    }

	
	void Update()
	{
        if(!animator.GetBool("IsDead"))
        {
            float distanceToPlayer = Vector2.Distance(aggroDetection.position, player.position);
        
            if(distanceToPlayer <= aggroRange)
            {
                isCloseToPlayer = true;
                CheckAggroState(distanceToPlayer);
            }
            else
            {
                isCloseToPlayer = false;
                PatrolMovement(startSpeed);
            }
        }
	}

	void PatrolMovement(float speed)
	{
		transform.Translate(Vector2.right * speed * Time.deltaTime);
		RaycastHit2D groundInfo = Physics2D.Raycast(groundDetection.position, Vector2.down, distanceToFloor);

		if(groundInfo.collider == false)
        {
            if(movingRight) rightLimitPositionX = groundDetection.position.x;
            else leftLimitPositionX = groundDetection.position.x;
            FlipMovement();
        } 
	}

    void MoveToDirection(float pointX, int direction)
    {
        while(transform.position.x != pointX)
        {
            if(direction == 0) transform.Translate(Vector2.right * speed * Time.deltaTime);
            else if(direction == 1) transform.Translate(Vector2.left * speed * Time.deltaTime);
            else Debug.Log("Direction input wrong");
        }
    }

	public void FlipMovement()
	{
		if(movingRight) {
			transform.eulerAngles = new Vector3(0, -180, 0);
			movingRight = false;
		} 
		else 
		{
			transform.eulerAngles = new Vector3(0,0,0);
			movingRight = true;
		}
	}

    private void CheckAggroState(float distanceToPlayer)
    {
        if(LookingToPlayer()) EnterAggroState(distanceToPlayer);
        else FlipMovement();
    }

	public void EnterAggroState(float distanceToPlayer)
    {
        
        if(HordeSystem.instance.NotifyHordeAggroEnterState(gameObject)) {
            HordeSystem.instance.SetPlayerPositionX(player.position.x);
            HordeSystem.instance.CheckHordeMemberPosition();
        }

        aggroState = true;
    	if(distanceToPlayer <= attackRange) 
    	{
    		Stop();
    		Attack();
    	} 
    	else {
            HordeSystem.instance.NotifyHordeAggroLeaveState();
            Run();
        }
    }

	void OnDrawGizmosSelected()
    {
    	Gizmos.color = Color.red;
    	Gizmos.DrawWireSphere(groundDetection.position, distanceToFloor);

    	Gizmos.color = Color.magenta;
    	Gizmos.DrawWireSphere(aggroDetection.position, aggroRange);
    }   

    public void Stop()
    {
    	PatrolMovement(0);
    }

    public void Attack()
    {
    	animator.SetTrigger("Attack");
    }

    public void Run()
    {
        aggroState = false;
    	animator.SetFloat("Speed", 0); //end first attack animation without movement
    	PatrolMovement(startSpeed);
    }
}
