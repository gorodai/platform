﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour
{
	public Animator animator;
	public Transform itemCatcher;
	public float itemCatcherRange;
	[SerializeField] private LayerMask _whatIsItem;

   
    void Update()
    {
       Collider2D[] item = Physics2D.OverlapCircleAll(itemCatcher.position, itemCatcherRange, _whatIsItem);
       for(int i=0; i < item.Length; i++)
        {
        	//Destroy(item[i].gameObject);
        	if(!animator.GetBool("IsJumping")) animator.SetTrigger("Win");
        }
    }

    void OnDrawGizmosSelected()
    {
        Color cyan = new Color(0f, 1f, 1f, 1f);
        Gizmos.color = cyan;
        Gizmos.DrawWireSphere(itemCatcher.position, itemCatcherRange);
    }

}
