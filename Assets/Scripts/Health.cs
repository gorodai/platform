﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{

	public int maxHealth = 100;
	public int currentHealth;
	public HealthBar healthBar;
	public Animator animator;


    // Start is called before the first frame update
    void Start()
    {
		currentHealth = maxHealth;
		healthBar.SetMaxHealth(maxHealth);
    }

    // Update is called once per frame
    void Update()
    {
		if(Input.GetKeyDown(KeyCode.Backspace))
		{
			TakeDamage(20);
		}
    }

	public void TakeDamage(int damage)
	{
		currentHealth -= damage;
		healthBar.SetHealth(currentHealth);
		if(currentHealth <= 0) Die();
	}	

	public void Die()
	{
		animator.SetBool("IsDead", true);
		//GameManager.GetIsInputEnabled() = false;
		GameManager.instance.EndGame();
	}
}
