﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HordeSystem : MonoBehaviour
{
	public Patrol patrol;
	//private GameObject[] enemies;
	public static HordeSystem instance;
	private List<GameObject> enemies = new List<GameObject>();
	private GameObject[] allies;
	private float hordeRange = 3.5f;
	private bool isPivot = false;
	private List<List<GameObject>> hordeList = new List<List<GameObject>>();
	private LayerMask enemyLayerMask;
	private bool hasAllies = false;
	private Dictionary<List<GameObject>, float[]> minMaxDict = new Dictionary<List<GameObject>, float[]>();
	private List<GameObject> aggroHorde;
	private bool aggroStateHorde;
	private float playerPositionX;
	private GameObject closestEnemy;
	private List<GameObject> aggroHorde;

	public void SetPlayerPositionX(float x)
	{
		playerPositionX = x;
	}

	public void NotifyHordeAggroEnterState(GameObject enemy)
	{
		foreach(List<GameObject> horde in hordeList)
		{
			if(horde.Contains(enemy.gameObject)) 
			{
				aggroStateHorde = true;
				closestEnemy = enemy; //TODO
				aggroHorde = horde;
			}
			else 
			{
				aggroStateHorde = false;
				closestEnemy = null;
			}
		}

	}

	public bool NotifyHordeAggroLeaveState()
	{
		aggroStateHorde = false;
	}


	private float GetRandomNumber(float minimum, float maximum)
	{ 
		var number = Random.Range(minimum, maximum);
		return number;
	}

    List<GameObject> FindGameObjectsInLayer(int layer)
    {
        var goArray = FindObjectsOfType(typeof(GameObject)) as GameObject[];
        var goList = new List<GameObject>();
        for (int i = 0; i < goArray.Length; i++)
        {
            if (goArray[i].layer == layer)
            {
                goList.Add(goArray[i]);
            }
        }
        if (goList.Count == 0)
        {
            return null;
        }
    	return goList;
    }

    private bool GetIsPivot()
    {
    	return isPivot;
    }

    void CreateHorde()
    {
    	enemyLayerMask = LayerMask.GetMask("Enemy");
    	enemies = FindGameObjectsInLayer(LayerMask.NameToLayer("Enemy"));
    	if(enemies.Count != 0)
    	{
    		var horde = new List<GameObject>();

    		for(int i=0; i<enemies.Count; i++)
    		{
    			Collider2D[] colliders = Physics2D.OverlapCircleAll(enemies[i].transform.position, hordeRange, enemyLayerMask);
    			
    			Debug.Log("início do bloco completo");
    	        Debug.Log(enemies[i].name.ToString());
    	        Debug.Log(colliders.Length);
    	        Debug.Log("Nome acima");

 				if(colliders.Length > 0)
 				{
 					foreach (Collider2D item in colliders)
    	            {
    	                 //if(item != enemies[i].transform.GetComponent<Collider>()) Debug.Log(item.ToString());
    	             	Debug.Log(item.gameObject.name.ToString());
    	             	if(enemies[i].name.ToString() != item.gameObject.name.ToString()) //ignore itself
    	             	{		 
    	             		if(!horde.Contains(item.gameObject)) 
    	             		{
    	             			Debug.Log("Vai entrar: " + item.gameObject.name.ToString());
    	             			horde.Add(item.gameObject);
    	             			enemies.Remove(item.gameObject);
    	             			hasAllies = true;
    	             		} 
    	             		else 
    	             		{
    	             			Debug.Log("Ja contem: " + item.gameObject.name.ToString());
    	             		}
    	             	} else Debug.Log("Colidiu consigo mesmo");
    	             }

    	             if(hasAllies) horde.Add(enemies[i]); // adiciona o proprio inimigo se ele teve algum aliado adicionado a horda
    	             hasAllies = false;

 				} else Debug.Log("No horde ally detected");

 				Debug.Log("start horde set");
 				if(!hordeList.Contains(horde) && horde.Count != 0) hordeList.Add(horde);
 				
 				foreach (List<GameObject> item in hordeList)
 				{
 					Debug.Log(item);
 					foreach(GameObject enemy in item) Debug.Log(enemy.transform.position);
 				}

 				Debug.Log("end horde set");
 				//horde.Clear(); 
 				horde = new List<GameObject>();
 				Debug.Log("fim do bloco completo");
    		}	
    	} else Debug.Log("No enemies loaded.");
    	
    	Debug.Log(enemies);
    }

    void CreateInstance()
    {
    	if(instance == null) instance = this;
    	else 
    	{
    		if(instance != this) Destroy(gameObject);
    	}
    	DontDestroyOnLoad(gameObject);
    }

    void DistanceBetweenHordeMembers(List<GameObject> horde)
    {
    	foreach(List<GameObject> horde in hordeList)
    	{
    		var minDistance = float.MaxValue;
    		var maxDistance = 0f;
    		var distance = 0f;
    		for(int i=0; i<horde.Count; i++)
    		{
    			
    			if(i != horde.Count-1) distance = Mathf.Abs(horde[i+1].transform.position.x - horde[i].transform.position.x);

    			if(distance > maxDistance) maxDistance = distance;
    			if(distance < minDistance) minDistance = distance;

    			if(i == horde.Count-1)
    			{
    				Debug.Log(maxDistance);
    				Debug.Log(minDistance);
    			}
    		}
    		float[] minMaxArray = { minDistance , maxDistance }; 
    		minMaxDict.Add(horde, minMaxArray);

    		foreach(float value in minMaxDict[horde])
    		{
    			Debug.Log(value);
    		}

    		Debug.Log(GetRandomNumber(minMaxDict[horde][0], minMaxDict[horde][1]));
 

    	}
    }

    void Awake()
    {
    	CreateInstance();
    	CreateHorde();
    	DistanceBetweenHordeMembers();
    }


    // Update is called once per frame
    void Update()
    {
    	//SetPivot();
    	//Debug.Log(transform.position.x);
    	if(aggroStateHorde) //verificar se é o pivo antes de entrar aqui
    	{
    		AdjustFormation(aggroHorde);
    		//AdjustAttack();
    	}    
    }

    void AdjustFormation(List<GameObject> hordeFormation)
    {
 		//verificar se: estão sobrepostos, estao muito distantes, estão muito proximos, 
 		//estão fora dos limites, estão muito proximos do jogador, estão por cima do jogador, estão muito distantes do player
    	foreach(float value in minMaxDict[hordeFormation])
    	{
    		Debug.Log(value);
    	}

    	foreach(List<GameObject> horde in hordeList)
    	{
    		var minDistance = float.MaxValue;
    		var maxDistance = 0f;
    		var distance = 0f;
    		for(int i=0; i<horde.Count; i++)
    		{
    			//sortear valores
    			//fazer ate horde.Count-1
    			//decisao aleatoria: move o primeiro, move o segundo, move ambos
    			//quem moveu nao pode mexer mais!
    			//verificar se estão dentro das regras, senao refaz o processo

    			float randomDistance = GetRandomNumber(minMaxDict[horde][i], minMaxDict[horde][i+1]);

    			int[] options = new float[0, 1, 2];
 				int randomIndex = Random.Range(0, 2);
 				int randomOption = options[randomIndex];

 				int[] numbersDirection = new int[0, 1];
 				int randomDirectionIndex = Random.Range(0,1);
 				int randomDirection = direction[randomDirectionIndex];

 				switch (randomOption)
 				{
 					case 0:
 						//verificar se a posição futura é a mesma do player (saber se a horda está a esquerda ou direita do player)
 						float pointX = randomDistance + horde[i].transform.position.x;
 						if(horde[i+1].transform.position.x == horde[i].transform.position.x) horde[i].transform.GetComponent<Patrol>().MoveToDirection(pointX, randomDirection); 
    					else if(Mathf.Abs(horde[i+1].transform.position.x - horde[i].transform.position.x) > minMaxDict[horde][1]) horde[i].transform.GetComponent<Patrol>().MoveToDirection(minMaxDict[horde][1], randomDirection);
    					else if(Mathf.Abs(horde[i+1].transform.position.x - horde[i].transform.position.x) < minMaxDict[horde][0]) horde[i].transform.GetComponent<Patrol>().MoveToDirection(minMaxDict[horde][1], randomDirection);
 						
 					break;

 					case 1:
 						//verificar se a posição futura é a mesma do player (saber se a horda está a esquerda ou direita do player)
 						float pointX = randomDistance + horde[i+1].transform.position.x;
 						if(horde[i+1].transform.position.x == horde[i].transform.position.x) horde[i+1].transform.GetComponent<Patrol>().MoveToDirection(pointX, randomDirection); 
    					else if(Mathf.Abs(horde[i+1].transform.position.x - horde[i].transform.position.x) > minMaxDict[horde][1]) horde[i+1].transform.GetComponent<Patrol>().MoveToDirection(minMaxDict[horde][1], randomDirection);
    					else if(Mathf.Abs(horde[i+1].transform.position.x - horde[i].transform.position.x) < minMaxDict[horde][0]) horde[i+1].transform.GetComponent<Patrol>().MoveToDirection(minMaxDict[horde][1], randomDirection);
 					break;

 					case 2:
 						float pointX = randomDistance + horde[i+1].transform.position.x;
 						if(horde[i+1].transform.position.x == horde[i].transform.position.x) horde[i+1].transform.GetComponent<Patrol>().MoveToDirection(pointX, randomDirection); 
    					else if(Mathf.Abs(horde[i+1].transform.position.x - horde[i].transform.position.x) > minMaxDict[horde][1]) horde[i+1].transform.GetComponent<Patrol>().MoveToDirection(minMaxDict[horde][1], randomDirection);
    					else if(Mathf.Abs(horde[i+1].transform.position.x - horde[i].transform.position.x) < minMaxDict[horde][0]) horde[i+1].transform.GetComponent<Patrol>().MoveToDirection(minMaxDict[horde][1], randomDirection);
 						horde[i].transform.GetComponent<Patrol>().MoveToDirection();
 						horde[i+1].transform.GetComponent<Patrol>().MoveToDirection();
 					break;
 				}
    			
    		}
    	}
    }		

    void AdjustAttack()
    {

    }
}
