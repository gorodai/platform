﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

	public CharacterController2D controller;
	public Animator animator;
	public float horizontalMove = 0f;
	public float runSpeed = 40f;
	bool jump = false;
	bool crouch = false;
    private float pushbackSpeed;

    void Update()
    {

        if(GameManager.GetIsInputEnabled())
        {
            //specify direction and speed 
            if(!animator.GetBool("IsDead")) 
            {
                horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
                animator.SetFloat("Speed", Mathf.Abs(horizontalMove));
            } 

            if(crouch && Input.GetButtonDown("Jump"))
            {
                crouch = false;
                animator.SetBool("IsCrouching", false); 
            }

            if (!crouch && Input.GetButtonDown("Jump") && !animator.GetBool("IsJumping") && !jump) 
            {
                jump = true;
                animator.SetBool("IsJumping", true);
            }

            if(Input.GetButtonDown("Crouch")) 
            {
                crouch = !crouch;
                animator.SetBool("IsCrouching", crouch);
            }    
        }
    }

    
    
    public void OnLanding() 
    {
    	jump = false;
    	animator.SetBool("IsJumping", false);
    }

    public void OnCrouching(bool IsCrouching) 
    {
    	crouch = IsCrouching;
    	animator.SetBool("IsCrouching", IsCrouching);
    }

    public void Pushback(float pushback)
    {
        // front attacks
        if(controller.GetFacingRight()) horizontalMove = pushback * (-1);
        else horizontalMove = pushback;

        controller.Move(pushback * 15, false, false, true);
        jump = false;
    }

   
    void FixedUpdate() 
    {
    	// no jump no crouch
    	controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump, false);
    	jump = false;
    }
}
