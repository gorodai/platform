﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollow : MonoBehaviour
{
	/*public Transform PlayerTransform;
	private Vector3 _cameraOffset;
    public bool LookAtPlayer = false;

    [Range(0.01f, 1.0f)]
    public float SmoothFactor = 0.5f;

    void Start()
    {
    	_cameraOffset = transform.position - PlayerTransform.position;
    }

    void LateUpdate()
    {
    	Vector3 newPos = PlayerTransform.position + _cameraOffset;
    	transform.position = Vector3.Slerp(transform.position, newPos, SmoothFactor);
        
        if(LookAtPlayer)
            transform.LookAt(PlayerTransform);
    }*/
    [SerializeField]
    private Transform targetToFollow;

    void Update()
    {
        // y 9 -1
        // x -9 24
        //TODO use background size with adjustments intead of constants
        transform.position = new Vector3( 
            Mathf.Clamp(targetToFollow.position.x, 0f, 24f),
            Mathf.Clamp(targetToFollow.position.y, 0f, 9f),
            transform.position.z);
    }
}
