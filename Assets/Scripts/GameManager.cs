﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public bool isPaused;

    public List<Item> items = new List<Item>(); //what kind
    public List<int> itemNumbers = new List<int>();//how many
    public GameObject[] slots;
    public Dictionary<Item, int> itemDict = new Dictionary<Item, int>();
    private static bool isInputEnabled = true;
    bool gameHasEnded = false;
    public UIManager uiManager;
    public GameObject rainAudio;

    private void Awake()
    {
    	if(instance == null) instance = this;
    	else 
    	{
    		if(instance != this) Destroy(gameObject);
    	}
    	DontDestroyOnLoad(gameObject);
    }

    public static bool GetIsInputEnabled()
    {
        return isInputEnabled;
    }

    public static void SetIsInputEnabled(bool enabled)
    {
        isInputEnabled = enabled;
    }

    private void Start()
    {
    	DisplayItems();
        GameManager.isInputEnabled = true;
    }

    private void DisplayItems() //improve
    {
    	//Debug.Log(items);
    	//Debug.Log(slots);
    	for(int i=0; i<items.Count; i++)
    	{
    		//change item image
    		slots[i].transform.GetChild(0).GetComponent<Image>().color = new Color (1,1,1,1);
    		slots[i].transform.GetChild(0).GetComponent<Image>().sprite = items[i].itemSprite;
    		//change slot counter
    		slots[i].transform.GetChild(1).GetComponent<Text>().color = new Color(1,1,1,1);
    		slots[i].transform.GetChild(1).GetComponent<Text>().text = itemNumbers[i].ToString();
    		//change delete button
    		slots[i].transform.GetChild(2).gameObject.SetActive(true);
    	}

    	for(int i=0; i<slots.Length; i++)
    	{
    		if(i<items.Count)
    		{
    			//change item image
    			slots[i].transform.GetChild(0).GetComponent<Image>().color = new Color (1,1,1,1);
    			slots[i].transform.GetChild(0).GetComponent<Image>().sprite = items[i].itemSprite;
    			//change slot counter
    			slots[i].transform.GetChild(1).GetComponent<Text>().color = new Color(1,1,1,1);
    			slots[i].transform.GetChild(1).GetComponent<Text>().text = itemNumbers[i].ToString();
    			//change delete button
    			slots[i].transform.GetChild(2).gameObject.SetActive(true);
    		}
    		else 
    		{
    			//change item image
    			slots[i].transform.GetChild(0).GetComponent<Image>().color = new Color (1,1,1,0);
    			slots[i].transform.GetChild(0).GetComponent<Image>().sprite = null;
    			//change slot counter
    			slots[i].transform.GetChild(1).GetComponent<Text>().color = new Color(1,1,1,0);
    			slots[i].transform.GetChild(1).GetComponent<Text>().text = null;
    			//change delete button
    			slots[i].transform.GetChild(2).gameObject.SetActive(false);
    		}
    	}
    }

    public void AddItem(Item _item)
    {
    	Debug.Log(_item);
    	Debug.Log(items[0]);
    	if(!items.Contains(_item)) 
    	{
    		items.Add(_item);
    		itemNumbers.Add(1);
    	} 
    	else 
    	{
    		for(int i=0; i<items.Count; i++)
    		{
    			if(_item == items[i])
    			{
    				itemNumbers[i]++;
    			}
    		}
    	}
    	DisplayItems();
    }

    public void RemoveItem(Item _item) 
    {
    	if(items.Contains(_item))
    	{
    		for(int i=0; i<items.Count; i++)
    		{
    			if(_item == items[i])
    			{
    				itemNumbers[i]--;
    				if(itemNumbers[i] == 0)
    				{
    					items.Remove(_item);
    					itemNumbers.Remove(itemNumbers[i]);
    				}
    			}
    		}
    	} else Debug.Log("There is no " + _item + "in bag");
    	
    	DisplayItems();
    }

    public void EndGame()
    {
        if(!gameHasEnded)
        {
            gameHasEnded = true;
            Debug.Log("Game over");
            
            uiManager.Invoke("GameOverScreen", 5f); 
            Invoke("PlayGameOverAudio", 5f);
        }
    }

    public void PlayGameOverAudio()
    {
        rainAudio.gameObject.GetComponent<AudioSource>().Play();
    }

    public void Update()
    {
        if(gameHasEnded && Input.GetButtonDown("Restart")) 
        {
            Debug.Log("ENTROU");
            Restart();
        }
    }

    void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}