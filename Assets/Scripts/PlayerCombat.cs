﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : MonoBehaviour
{
	[SerializeField] private LayerMask whatIsEnemy;
    private float cooldownAttack = 0.3f;
    private float startCooldownAttack;
    public Transform heavyAttackPosition;
    public Transform lightAttackPosition;
    public float lightAttackRange;
    public float heavyAttackRange;
    public float executionRange = 4;
    public int heavyDamage = 100;
    public int lightDamage = 20;
    public int executionDamage = 200;
    public Animator animator;
    public float heavyDurationShake = 0.15f;
    public float heavyMagnitudeShake = 0.4f;
    public CamShake camShake;
    public Animator bloodAnimation;
    public Health health;
    public PlayerMovement playerMovement;
    public GameObject parry;
    public Animator enemyStunned; //TODO
    public UIManager uiManager;


    void Start()
    {
        //Debug.Log(bloodAnimation.GetComponent<SpriteRenderer>());
    }

    void Update()
    {

        if(GameManager.GetIsInputEnabled())
        {
            // cheer pose
            if(Input.GetButtonDown("Win")) animator.SetTrigger("Win");
            //show hair pose
            if(Input.GetButtonDown("Beauty")) animator.SetTrigger("Beauty");
        }
        
        //combat inputs
        if(cooldownAttack <= 0) 
        {
            cooldownAttack = startCooldownAttack;
                
            if(Input.GetButtonDown("LightAttack") && GameManager.GetIsInputEnabled())
            {
                animator.SetTrigger("LightAttack");
                Collider2D[] enemies = Physics2D.OverlapCircleAll(lightAttackPosition.position, lightAttackRange, whatIsEnemy);
                for(int i=0; i < enemies.Length; i++)
                {
                    enemies[i].GetComponent<Enemy>().TakeDamage(lightDamage);
                }
            }

            if(Input.GetButtonDown("HeavyAttack") && !animator.GetBool("IsBlocking") && GameManager.GetIsInputEnabled()) //TODO
            {
                StartCoroutine(camShake.Shake(heavyDurationShake, heavyMagnitudeShake));
                if(enemyStunned.GetBool("IsStunned")) //bug onde pode dar parry em outro alvo
                {
                    uiManager.ShowInteractionText("It is not even worthy to be cooked.");
                    animator.SetTrigger("Execution");
                    Collider2D[] enemies = Physics2D.OverlapCircleAll(heavyAttackPosition.position, executionRange, whatIsEnemy);
                    for(int i=0; i < enemies.Length; i++)
                    {
                        enemies[i].GetComponent<Enemy>().TakeDamage(executionDamage);
                    }
                }
                else 
                {
                    animator.SetTrigger("HeavyAttack");
                    Collider2D[] enemies = Physics2D.OverlapCircleAll(heavyAttackPosition.position, heavyAttackRange, whatIsEnemy);
                    for(int i=0; i < enemies.Length; i++)
                    {
                        Debug.Log(enemies[i].GetComponent<Enemy>());
                        enemies[i].GetComponent<Enemy>().TakeDamage(heavyDamage);
                    }
                }
            }

            //block & parry
            if(animator.GetBool("IsBlocking")) Parry();

            if(Input.GetButtonDown("Block") && GameManager.GetIsInputEnabled())
            {
                animator.SetBool("IsBlocking", !animator.GetBool("IsBlocking"));
                Parry();
            }

            } else cooldownAttack -= Time.deltaTime;

    }

    public void Parry()
    {
        if(Input.GetButtonDown("HeavyAttack"))
        {
            animator.SetTrigger("Parry");
            if(parry) parry.GetComponent<BoxCollider2D>().enabled = true;
            uiManager.ShowInteractionText("Try to touch me if you dare.");
        }
    }

    public void TakeSlashDamage(int damage, float pushback)
    {
        if(!animator.GetBool("IsBlocking"))
        {
            //blood animation
            bloodAnimation.SetTrigger("Slash");
            //hurt animation / hitstun
            animator.SetTrigger("Hurt");
            //damage
            health.TakeDamage(damage);
            //knockback
            playerMovement.Pushback(pushback);    
        } 
        else
        {
            //block animation effect
            //open block
            animator.SetBool("IsBlocking", false);
        } 
    }

    void OnDrawGizmosSelected()
    {
    	Gizmos.color = Color.red;
    	Gizmos.DrawWireSphere(lightAttackPosition.position, lightAttackRange);
        Color pink = new Color(0.81f, 0.41f, 0.79f, 1f);
        Gizmos.color = pink;
        Gizmos.DrawWireSphere(heavyAttackPosition.position, heavyAttackRange);
    }
}
