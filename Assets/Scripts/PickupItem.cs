﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupItem : MonoBehaviour
{
	public Item itemData;
	public UIManager uiManager;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            if(GameManager.instance.items.Count < GameManager.instance.slots.Length)
            {
                Destroy(gameObject);
                GameManager.instance.AddItem(itemData);
                uiManager.ShowInteractionText(itemData.textInteraction);
            } else Debug.Log("Inventory full");    
        }
    }
}
