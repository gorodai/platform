﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatyEffect : MonoBehaviour
{
    private float amplitude = 0.2f;
    private float speed = 0.09f;
    private float originalPosition;

    // Update is called once per frame
    void FixedUpdate()
    {
      	// Save the y position prior to start floating (maybe in the Start function):
   		var y0 = transform.position.y;
 		// Put the floating movement in the Update function:
 		transform.position = new Vector3( 
            transform.position.x,
            y0+amplitude*Mathf.Sin(speed),
            transform.position.z);
    }
}