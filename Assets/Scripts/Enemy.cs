﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
	public int health = 200;
    public Animator animator;
    public PlayerCombat playerCombat;
    public int damage = 20;
    public float pushback = 1.5f;

    void Start()
    {
        //anim = GetComponent<Animator>();
        //anim.SetBool("isRunning", true);
    }

    void Update()
    {
        //transform.Translate(Vector2.left * speed * Time.deltaTime);
    }

    public void TakeDamage(int damage) 
    {
        animator = GetComponent<Animator>();
        var isDead = animator.GetBool("IsDead");
        if(!isDead)
        {
            health -= damage;
            if(health <= 0) Die();
        }
    }

    public void Die()
    {
        Debug.Log("Enemy died!");
        animator.SetBool("IsDead", true);
        GetComponent<Collider2D>().enabled = false;
        GetComponent<Rigidbody2D>().simulated = false;
        this.enabled  = false;

    }

    public void OnTriggerEnter2D(Collider2D collider)
    {
        //only damage Player, only collides with torso box
        Debug.Log(collider.gameObject.name);
        if(collider && collider.gameObject.name == "parry") animator.SetBool("IsStunned", true);
        else 
        {
            if(collider && collider.gameObject.name == "Player" && collider.GetType() == typeof(BoxCollider2D)) playerCombat.TakeSlashDamage(damage, pushback);
            //if(collider && collider.gameObject.name == "Player") Debug.Log(collider.GetType());
        }
    }
}


    